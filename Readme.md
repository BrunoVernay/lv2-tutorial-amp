Learning LV2 plugin

Following https://github.com/sjaehn/lv2tutorial 


## Build

Pre-requisite for the `lv2.h` and such
`sudo dnf install lv2-devel`

(There is a `lv2-c++-tools` package, but looks very specific. Not needed.)


Build with: 
`gcc -fvisibility=hidden -fPIC -Wl,-Bstatic -Wl,-Bdynamic -Wl,--as-needed -shared -pthread $(pkg-config --cflags lv2) -lm $(pkg-config --libs lv2) my-amp.c -o my-amp.so`

Install
```
mkdir ~/.lv2/my-amp/
cp manifest.ttl my-amp.ttl my-amp.so ~/.lv2/my-amp/

```


## Test

pre-requisites: `sudo dnf install lv2lint jalv`

1. Linter
`lv2lint https://gitlab.com/BrunoVernay/lv2-tutorial-amp`
2. Test `jalv https://gitlab.com/BrunoVernay/lv2-tutorial-amp`
3. Test in Ardour ...

(`sord_validate` was not helpful as of october 2021. Too many errors, too few documentation.)